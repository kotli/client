package org.kotlin.client

import java.net.Socket

class Robot(val addr:String, val port: Int){
    fun sendToRobot(msg: String){
        val sock = Socket(addr, port)
        sock.use {
            it.outputStream.write(msg.toByteArray(
                    Charsets.UTF_8
            ))
            it.outputStream.flush()
            var aob = ByteArray(1024)
            it.inputStream.read(aob)
            println(aob.toString(Charsets.UTF_8))
        }
    }
}
